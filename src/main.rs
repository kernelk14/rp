use std::vec::Vec;

// struct Span {{{
#[allow(dead_code)]
struct Span {
    start: usize,
    end: usize,
}
// }}}
// enum Token {{{
#[allow(dead_code)]
#[derive(Debug)]
enum Token {
    TokenKind(String),
    Text,
    OpenParen,
    CloseParen,
    SingleQuote,
    DoubleQuote,
}

fn FindToken(kind: Token) {
    let ft = match kind {
        Token::TokenKind(t) => Token::tokenKind(&t),
        Token::OpenParen => Token::tokenKind("("),
        Token::CloseParen => Token::tokenKind(")"),
        Token::SingleQuote => Token::tokenKind("'"),
        Token::DoubleQuote => Token::tokenKind("\""),
        _ => println!("FindToken Failed.")
    };
}
// }}}
// impl Token {{{
#[allow(dead_code)]
// #[derive(Debug)]
impl Token {
    pub fn tokenKind(identifier: &str) {
        match identifier {
            "write" => println!("I saw a write keyword."),
            "(" => println!("I saw an open parentheses."),
            ")" => println!("I saw a close parentheses."),
            "\"" => println!("String Literal"),
            &_ => eprintln!("Unknown Token {}", identifier)
        }
    }
    fn gen_tok() {
        eprintln!("Token Generation Not Implemented.")
    }
}

fn evaluator() {

}
// }}}
fn main() {
    // println!("Hello, world!");
    
    let ident: Vec::<String> = [].to_vec();
    
    let program = "write('Hello World')";
    for char in program.chars().next() {
        match program {
            "(" => Token::tokenKind("("),
            "write" => Token::tokenKind("write"),
            ")" => Token::tokenKind(")"),
            "\"" => Token::tokenKind("\""),
            &_ => {
                return Token::tokenKind(program);
            },
        }
        // println!("{:?}", char);        
    }
    println!("{:?}", ident);
}
